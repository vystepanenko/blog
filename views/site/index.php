<?php

/* @var $this yii\web\View */

$this->title = 'My Test Task';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Test task: BLOG</h1>

        <p class="lead">Source code you can find on <a href="https://bitbucket.org/vystepanenko/blog">BitBucket</a></p>

        <p class="lead">Only registered users can leave comment</p>

        <p class="lead">Commentary mange <a href="/manage">Here</a></p>

    </div>

    <div class="body-content">


    </div>
</div>
