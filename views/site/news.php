<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'News';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php for ($i = 0; $i < count($posts); $i++) {
        echo '<div class="alert alert-success">';
        echo '<h1>Title: ' . $posts[$i]['title'] . '</h1>';
        echo Html::img('@web/images/'.$posts[$i]['picture']);
        echo '<p>Content: ' . $posts[$i]['content'] . '</p>';
        echo '<p><strong>Date: ' . $posts[$i]['date'] . '</strong></p>';
        echo '<p>Username: <i>' . $posts[$i]['bloguser'] . '</i></p>';
        if ($posts[$i]['showcomment'] == 1) {
            echo \yii2mod\comments\widgets\Comment::widget([
                'model' => $model[$i],
                'entityIdAttribute' => 'id',
                'formId' => 'comment-form' . $i,
                'pjaxContainerId' => 'unique-pjax-container-' . $i,

            ]);
        }
        echo '</div>';
    } ?>
    <?php ?>
</div>