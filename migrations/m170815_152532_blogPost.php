<?php

use yii\db\Migration;

/**
 * Class m170815_152532_BlogPost
 */
class m170815_152532_blogPost extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('blogPost', [
            'id' => 'pk',
            'title' => 'VARCHAR(255)',
            'content' => 'TEXT DEFAULT NULL',
            'bloguser' => 'VARCHAR(255) DEFAULT NULL',
            'picture' => 'VARCHAR(255) DEFAULT NULL',
            'date' => 'DATE DEFAULT NULL',
            'showcomment' => 'int(1) DEFAULT NULL',
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('blogPost');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170815_152532_BlogPost cannot be reverted.\n";

        return false;
    }
    */
}
