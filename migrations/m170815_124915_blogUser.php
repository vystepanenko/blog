<?php

use yii\db\Migration;

/**
 * Class m170815_124915_blogUser
 */
class m170815_124915_blogUser extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('blogUser', [
            'id' => 'pk',
            'name' => 'VARCHAR(32) DEFAULT NULL',
            'username' => 'VARCHAR(255) DEFAULT NULL',
            'password' => 'VARCHAR(255) DEFAULT NULL',
            'authKey' => 'VARCHAR(255) DEFAULT NULL',
            'role' => 'VARCHAR(32) DEFAULT NULL',
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('blogUser');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170815_124915_blogUser cannot be reverted.\n";

        return false;
    }
    */
}
