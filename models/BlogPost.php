<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "blogPost".
 *
 * @property int $id
 * @property string $title
 * @property string $content
 * @property int $bloguser
 * @property string $picture
 * @property string $date
 * @property int $showcomment
 */
class BlogPost extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blogPost';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content', 'bloguser'], 'string'],
            [['showcomment'], 'integer'],
            [['date'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['title'], 'required'],
            [['picture'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'content' => 'Content',
            'bloguser' => 'Bloguser',
            'picture' => 'Picture',
            'date' => 'Date',
            'showcomment' => 'siComment',
        ];
    }

    public function findAllPost()
    {
        return static::find()
//            ->where(['>', 'id', '0'])
            ->all();
    }

    public function findUserPost($username)
    {
        return static::findAll(['bloguser' => $username]);
    }

}
