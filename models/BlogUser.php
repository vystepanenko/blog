<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\base\NotSupportedException;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\imagine\Image;

/**
 * This is the model class for table "blogUser".
 *
 * @property int $id
 * @property string $name
 * @property string $username
 * @property string $password
 * @property string $authkey
 * @property string $role
 */
class BlogUser extends ActiveRecord implements IdentityInterface
{

    public $hashPassword = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blogUser';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'role'], 'string', 'max' => 32],
            [['username', 'password',], 'string', 'max' => 255,],
            [['username', 'password'], 'required'],
            [['username'], 'unique'],
            [['password'], 'string', 'min' => 6]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Full Name',
            'username' => 'Nick name',
            'password' => 'Password',
            'authkey' => 'Authkey',
            'role' => 'Role',
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public function getId()
    {
        return $this->id;
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    public static function findById($id)
    {
        return static::findOne(['id' => $id]);
    }

    public function validatePassword($password)
    {
        return \Yii::$app->security->validatePassword($password, $this->password);
    }

    public function getAuthKey()
    {
        return $this->authKey;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->hashPassword) {
                $this->password = \Yii::$app->security->generatePasswordHash($this->password, 10);
            }
            return true;
        } else {
            return false;
        }
    }

}
